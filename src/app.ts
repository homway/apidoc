import express from 'express'
import swaggerUi from 'swagger-ui-express'
import YAML from 'yamljs'

const swaggerDocument = YAML.load(__dirname + '/../reference/admin.yaml')
const app = express();

app.use("/api", swaggerUi.serve, swaggerUi.setup(swaggerDocument))

export default app;