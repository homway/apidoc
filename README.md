---
title: OpenAPI 3.0.0
tags: openapi
---

# OpenAPI 3.0:

## Stoplight: 

* 官網: [StopLight](https://www.stoplight.io/)
* Workspace: [homway stoplight](https://homway.stoplight.io/)

* 編輯OpenAPI 3.0文件
* 版本控制, 檔案資料夾為: reference
* Gitlab帳號登入, Azure Devops帳號要收費，以後在說

## Prism-Cli: Mock API Server
```
npm i @stoplight/prism-cli
npx prism mock -d -p 4010 reference/admin.yaml
```
### Prism-Cli 參數:
  * -d: 動態回傳 Mock 資料, 不設定則回傳固定值
  * -p: 指定listen port號, 預設為4010
  * -h: 使用docker版本需指定 0.0.0.0, 預設為127.0.0.1

## Swagger-UI-Express
```
npm i express \
  swagger-ui-express \
  yamljs

```

```
import express from 'express'
import swaggerUi from 'swagger-ui-express'
import YAML from 'yamljs'

const swaggerDocument = YAML.load(__dirname + '/../reference/admin.yaml')
const app = express();

app.use("/api", swaggerUi.serve, swaggerUi.setup(swaggerDocument))

app.listen(3000);
```
## Insomnia: 測試API + 版本控制
* brew install --cask insomnia
* 打開[insomnia plugin](https://insomnia.rest/plugins/insomnia-plugin-randomnumber)新增[randomNumber](https://insomnia.rest/plugins/insomnia-plugin-randomnumber)
* GitLab新增Personal User Token
* Insomnia 右上角 create 下拉 -> git clone -> 輸入GitLab資訊
* 正上方分三個區域
* DESIGN: OPEN API 工具
* DEBUG: REST API 工具
* TEST: 批次執行REST API 工具
* DEBUG 左上角 OpenAPI env -> 新增環境和範本變數

# 更新步驟
* 先用StopLight編輯OpenAPI 3.0 文件，使用YAML格式
* 編修完成後同步到GitLab
* git pull後npm run start
* insomnia git pull 後 到test介面測試調整
* insomnia 測試案例如有調整記得同步會GitLab

# TODO
* Rest API的input和output參數屬性還要看文件
* Auth驗證檢查還要測試
* Rest API的Status Code還要規劃錯誤碼，404待討論
* OpenApi文件和後端完成的程式碼驗證方式還要測試

# HTTP Status Code
url: https://zh.wikipedia.org/wiki/HTTP%E7%8A%B6%E6%80%81%E7%A0%81
## Create

- 200: OK
- 400: Bad Request
- 401: Unauthorized
- 500: Internal Server Error

## Read

- 200: OK
- 400: Bad Request
- 401: Unauthorized
- 500: Internal Server Error

## Update

- 200: OK
- 400: Bad Request
- 401: Unauthorized
- 404: Not Found
- 500: Internal Server Error

## Delete

- 200: OK
- 400: Bad Request
- 401: Unauthorized
- 404: Not Found
- 500: Internal Server Error

